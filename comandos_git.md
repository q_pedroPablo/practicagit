# Comandos GIT

## Configuración

| Comando     | Parámetro(s)                | Descripción                       |
| ----------- | --------------------------- | --------------------------------- |
| git config  | --global user.name <nombre> | Se configura el nombre de usuario |
|             | --global user.email <email> | Se configura el email de usuario  |
|             | -global --list              | Listar las configuraciones de git |


## Inicialización

| Comando                          | Parámetro(s)  | Descripción                                  |
| -------------------------------- | ------------- | -------------------------------------------- |
| git init                         |               | Inicializa un proyecto con git               |
| got clone <ruta del repositorio> |               | Clona un proyecto ya existente en nuestra pc |
 

## Proceso de guardado

| Comando    | Parámetro(s)                | Descripción                                                                |
| -----------| --------------------------- | -------------------------------------------------------------------------- |
| git status |                             | Lista los archivos con cambios                                             |
| git add    | <nombre del archivo>        | Agrega un archivo a un área de preparado                                   |
|            | .                           | Agrega todos los archivos modificados de la carpeta a el área de preparado |
|            | -A                          | Agrega todos los archivos modificados a el área de preparado               |
| git commit |                             | Añade los cambios al repositorio                                           |
|            | -m <Descripción de cambios> | Añade el mensaje con la descripción de los cambios                         |
|            | --amend                     | Corrije el mensaje del último commit                                       |


## Historial de cambios

| Comando           | Parámetro(s)       | Descripción                                                 |
| ----------------- | ------------------ | ----------------------------------------------------------- |
| git log           |                    | Muestra el historial de commits                             |
|                   | --oneline          | Formato del historial en una sola línea                     |
|                   | --all              | Muestra todo el historial incluyendo los de otras ramas     |
| git diff          |                    | Muestra las diferencias entre los archivos modificados      |
| git revert        |                    | Revierte el último commit y crea un commit a partir de este |
| git checkout <id> |                    | Muestra el estado en el que estaba el repositorio           |
| got reset         |                    | Quita el último commit                                      |
|                   | --soft <id_commit> | Mantiene los cambios                                        |
|                   | --hard <id_commit> | Borra todo                                                  |

## Repositorios remotos

| Comando                                  | Parámetro(s) | Descripción                                                  |
| ---------------------------------------- | ------------ | ------------------------------------------------------------ |
| git remote add origin <ruta repositorio> |              | Vincula el repositorio local con un repositorio remoto       |
| git push                                 |              | Sube los cambios al repositorio remoto                       |
| git pull                                 |              | Baja los cambios del repositorio remoto al repositorio local |


## Ramas

| Comando                             | Parámetro(s)                 | Descripción                         |
| ----------------------------------- | ---------------------------- | ----------------------------------- |
| git branch                          |                              | Lista las ramas del repositorio     |
|                                     | <nombre rama nueva>          | Crea una rama nueva                 |
|                                     | -m <rama vieja> <rama nueva> | Renombrar rama en repositorio local |
|                                     | -d o -D                      | Borrado de la rama                  |
| git checkout                        | <nombre de la rama>          | Cambiar a la rama seleccionada      |
| git push origin <nombre de la rama> |                              | Borrar rama en repositorio remoto   |
| git merge <nombre de la rama>       |                              | Combina dos ramas                   |
