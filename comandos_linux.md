# Comandos Linux

## Configuración


| Comando    | Parámetro(s)                          | Descripción                                 |
| ---------- | ------------------------------------- | ------------------------------------------- |
| ls         |                                       | Listar archivos y carpetas                  |
|            | -l                                    | Listar archivos y carpetas en formato largo |
|            | -la                                   | Muestra los archivos ocultos                |
| pwd        |                                       | Saber en que carpeta estoy                  |
| clear      |                                       | Limpia la terminal                          |
| sudo       |                                       | Ejecutar comandos con el superusuario       |
| more / cat | <nombre archivo>                      | Muestra el contenido del archivo            |
| touch      | <nombre archivo>                      | Crea un archivo nuevo vacío                 |
| vi / nano  | <nombre archivo>                      | Editar archivo                              |
| cd         |                                       | Cambiar de carpeta                          |
| mkdir      | <nombre carpeta>                      | Crear una carpeta                           |
| rm         | <nombre archivo>                      | Borrar archivo                              |
|            | -r <carpeta>                          | Borrar carpeta                              |
| mv         | <archivo> <ruta> <renombre archivo>   | Mover archivo o carpeta                     |
| su         | <usuario>                             | Cambiar de usuario                          |
| exit       |                                       | Salir de la terminal                        |
| chmod      | <modo octal> <archivo carpeta>        | Cambiar permisos                            |
| chown      | <usuario> <archivo>                   | Cambiar de propietario                      |
| tar        | <-cvfz> <nombre> <archivo(s) carpeta> | Comprime archivos para respaldo             |
|            | -c                                    | Crear archivo                               |
|            | -v                                    | Descripción                                 |
|            | -f                                    | Archivo                                     |
|            | -z                                    | Compresión con gzip                         |


### Referencias permisos

|---|---|---------|
| u |   | user    |
| g |   | group   |
| o |   | others  |
| r | 4 | read    |
| w | 2 | write   |
| x | 1 | execute |